using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace LS
{
    public class SpawnPelotas : MonoBehaviour
    {
        [Header("Instanciar pelota")]
        [SerializeField] GameObject Pelota;
        [SerializeField] Rigidbody Rb;

        [Header("Variables")]
        [SerializeField] Transform PuntoDeSpawn;
        [SerializeField] int cantidad;
        [SerializeField] float tiempo;

        [Header("Interfaz")]
        [SerializeField] Slider timeScale;
        [SerializeField] TextMeshProUGUI tiempoCount;
        [SerializeField] TMP_InputField ingresarTxt;

        [Header("Bool Color")]
        [SerializeField] bool RandomColor = false;

        private void Start()
        {
            timeScale.minValue = 0.5f;
            timeScale.maxValue = 20f;
            tiempoCount.text = Time.timeScale.ToString();
        }

        public void SetInputTExt(string text)
        {
            cantidad = int.Parse(text);
        }

        public void InstanciarPelotas()
        {
            for (int i = 0; i < cantidad; i++)
            {
                if (RandomColor)
                {
                    Color newCOlor = new Color(r: Random.Range(0f, 1f), g: Random.Range(0f, 1f), b: Random.Range(0f, 1f));
                    GameObject pelota = Instantiate(Pelota, PuntoDeSpawn.transform.position, PuntoDeSpawn.transform.rotation);
                    pelota.GetComponent<Renderer>().material.color = newCOlor;
                }
                else
                {
                    GameObject pelota = Instantiate(Pelota, PuntoDeSpawn.transform.position, PuntoDeSpawn.transform.rotation);
                }
            }  
        }

        public void TiempoMasRapido()
        {
            tiempo = timeScale.value;

            tiempoCount.text = tiempo.ToString("0.0");
            
            Time.timeScale = timeScale.value;
        }

        public void Reiniciar()
        {
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
        }

        public void CambiarBool()
        {
            RandomColor = !RandomColor;
        }
    
    }
}
